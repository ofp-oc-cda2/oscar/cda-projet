package api.tools.response;

import api.tools.specialModel.BasicResponse;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ResponseProvider {

    @Transactional
    public BasicResponse response(Integer status,String message, Object data) {
        BasicResponse response = new BasicResponse();

        response.setStatus(status);
        response.setMessage(message);
        response.setData(data);

        return response;
    }
    
}
