package api.tools.userService;

import api.models.User;
import api.tools.jwt.JwtTokenProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     *
     * @param username
     * @param password
     * @return
     */
    public String login(User user) {
        log.info(user.getPassword());
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getMail(), user.getPassword()));
        return jwtTokenProvider.createToken(user);
    }

    /**
     *
     * @param remoteUser
     * @return
     */
    public String refreshToken(User user) {
        return jwtTokenProvider.createToken(user);
    }

}