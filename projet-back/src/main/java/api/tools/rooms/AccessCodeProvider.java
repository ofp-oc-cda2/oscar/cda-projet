package api.tools.rooms;

import java.util.Random;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.crypto.bcrypt.BCrypt;

@Repository
public class AccessCodeProvider {

    @Transactional
    public String createCode() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 6;
        Random random = new Random();
    
        String generatedString = random.ints(leftLimit, rightLimit + 1)
          .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
          .limit(targetStringLength)
          .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
          .toString();
    
        return generatedString;
    }

    @Transactional
    public Integer encryptCode(String code) {
        
        Integer encryptedCode = code.hashCode();
    
        return encryptedCode;
    }
    
    @Transactional
    public Boolean checkCode(String code, String encryptedCode) {
        
        if(BCrypt.hashpw(code, BCrypt.gensalt()) == encryptedCode) {
            return true;
        } else {
            return false;
        }
    
    }
}
