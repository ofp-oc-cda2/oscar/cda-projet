package api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import api.models.Team;
import api.repository.TeamRepository;

@RestController
@RequestMapping(value = "/api")
public class TeamController {

    @Autowired
    private TeamRepository teamRepository;

    @GetMapping(value = "/teams")
    public ResponseEntity<List<Team>> getAllTeams() {

        List<Team> teams = teamRepository.getAllTeams();

        if(teams == null) {
            teams = new ArrayList<Team>();
        }

        ResponseEntity<List<Team>> response = ResponseEntity.ok(teams);

        return response;
        
    }

    
    
    
}
