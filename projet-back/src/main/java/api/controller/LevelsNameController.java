package api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import api.models.LevelsName;
import api.repository.LevelsNameRepository;

@RestController
@RequestMapping(value = "/api")
public class LevelsNameController {

    @Autowired
    private LevelsNameRepository levelsNameRepository;

    @GetMapping(value = "/levels")
    public ResponseEntity<List<LevelsName>> getAllLevelsNames() {

        List<LevelsName> levelsNames = levelsNameRepository.getAllLevelsNames();

        if (levelsNames == null) {
            levelsNames = new ArrayList<LevelsName>();
        }

        ResponseEntity<List<LevelsName>> response = ResponseEntity.ok(levelsNames);

        return response;

    }
    
}
