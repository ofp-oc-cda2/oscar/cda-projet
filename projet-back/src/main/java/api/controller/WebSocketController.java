package api.controller;

import api.models.ChatMessage;
import api.models.ChatMessage.MessageType;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class WebSocketController {

    private static final Logger log = LoggerFactory.getLogger(WebSocketController.class);

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/publicChatRoom")
    public ChatMessage sendMessage(@Payload ChatMessage chatMessage) {
        return chatMessage;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/topic/publicChatRoom")
    public ChatMessage addUser(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        // Add username in web socket session
        String content = null;
        if(chatMessage.getType() == MessageType.JOIN) {
            content = chatMessage.getSender() + " " + "is now connected";
        } else if (chatMessage.getType() == MessageType.LEAVE) {
            content = chatMessage.getSender() + " " + "is now disconnected";
        }
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        ChatMessage message = new ChatMessage();
        message.setType(chatMessage.getType());
        message.setContent(content);
        message.setSender(chatMessage.getSender());
        return message;
    }

    @MessageMapping("/chat.joinRoom")
    @SendTo("/game/{gameName}/{roomName}")
    public ChatMessage joinRoom(@Payload ChatMessage chatMessage, SimpMessageHeaderAccessor headerAccessor) {
        String content = null;
        if(chatMessage.getType() == MessageType.JOIN) {
            content = chatMessage.getSender() + " " + "joined the room.";
        } else if (chatMessage.getType() == MessageType.LEAVE) {
            content = chatMessage.getSender() + " " + "left the room.";
        }
        headerAccessor.getSessionAttributes().put("username", chatMessage.getSender());
        ChatMessage message = new ChatMessage();
        message.setType(chatMessage.getType());
        message.setContent(content);
        message.setSender(chatMessage.getSender());
        return message;
    }

    @MessageMapping("/chat.actionInRoom")
    @SendTo("/game/{gameName}/{roomName}")
    public ChatMessage actionInRoom(@Payload ChatMessage chatMessage) {
        return chatMessage;
    }

}
