package api.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import api.models.Rooms;
import api.repository.RoomsRepository;
import api.tools.jwt.JwtTokenProvider;
import api.tools.response.ResponseProvider;
import api.tools.rooms.AccessCodeProvider;
import api.tools.specialModel.BasicResponse;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

@RestController
@RequestMapping(value = "/api")
public class RoomsController {

    @Autowired
    private RoomsRepository roomRepository;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    ResponseProvider responseProvider;

    @Autowired
    AccessCodeProvider accessCodeProvider;

    @GetMapping(value = "/rooms")
    public ResponseEntity<BasicResponse> getRoomsByAccessCode(HttpServletRequest request, @RequestBody Rooms roomData) {

        String token = jwtTokenProvider.extractToken(request);
        Jws<Claims> validateToken = jwtTokenProvider.validateToken(token);
        if(validateToken == null) {
            return ResponseEntity.badRequest().body(responseProvider.response(400, "Token is not valid", null));
        }
        if(roomData.getCode() == null) {
            return ResponseEntity.badRequest().body(responseProvider.response(400, "You need to give a game code", null));
        }

        Rooms room = roomRepository.getRoomByAccessCode(accessCodeProvider.encryptCode(roomData.getCode()));
        room.setCode(roomData.getCode());

        return ResponseEntity.ok(responseProvider.response(200, "OK", room));
        
    }

    @PostMapping(value = "/rooms")
    public ResponseEntity<BasicResponse> createRoom(HttpServletRequest request, @RequestBody Rooms roomData) {

        String token = jwtTokenProvider.extractToken(request);
        Jws<Claims> validateToken = jwtTokenProvider.validateToken(token);
        if(validateToken == null) {
            return ResponseEntity.badRequest().body(responseProvider.response(400, "Token is not valid", null));
        }
        if(roomData.getGame() == null) {
            return ResponseEntity.badRequest().body(responseProvider.response(400, "Game is not specified for this room", null));
        }

        Object i = validateToken.getBody().get("id");
        Integer id = (Integer) i;
        String name = roomData.getName() != null ? roomData.getName() : "Party of " + validateToken.getBody().get("username").toString();
        String accessCode = accessCodeProvider.createCode();

        String room = roomRepository.createRooms(name, accessCodeProvider.encryptCode(accessCode), id, roomData.getGame());
        Rooms roomCreated = roomRepository.getRoomByAccessCode(accessCodeProvider.encryptCode(accessCode));

        return ResponseEntity.ok(responseProvider.response(200, room, roomCreated));
        
    }

}
