package api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import api.models.Word;
import api.repository.WordRepository;

@RestController
@RequestMapping(value = "/api")
public class WordController {

    @Autowired
    private WordRepository wordRepository;

    @GetMapping(value = "/words")
    public ResponseEntity<List<Word>> getAllWords() {

        List<Word> words = wordRepository.getAllWords();

        if(words == null) {
            words = new ArrayList<Word>();
        }

        ResponseEntity<List<Word>> response = ResponseEntity.ok(words);

        return response;
        
    }

    
}
