package api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import api.models.Games;
import api.repository.GamesRepository;

@RestController
@RequestMapping(value = "/api")
public class GamesController {

    @Autowired
    private GamesRepository gamesRepository;

    @GetMapping(value = "/get-all-games")
    public ResponseEntity<List<Games>> getAllGames() {

        List<Games> games = gamesRepository.getAllGames();

        if(games == null) {
            games = new ArrayList<Games>();
        }

        ResponseEntity<List<Games>> response = ResponseEntity.ok(games);

        return response;
        
    }

    @GetMapping(value = "/get-game/{id}")
    public ResponseEntity<Games> getGameById(@PathVariable("id") String id) {
        Games game = gamesRepository.getGameById(id);

        if(game == null) {
            game = new Games();
        }

        ResponseEntity<Games> response = ResponseEntity.ok(game);

        return response;
        
    }

}
