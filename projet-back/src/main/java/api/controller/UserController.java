package api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import api.models.User;
import api.tools.specialModel.UserLoged;
import api.tools.userService.UserService;
import api.tools.fileStorage.FilesStorageService;
import api.tools.password.PasswordUtil;
import api.tools.specialModel.BasicResponse;
import api.tools.response.ResponseProvider;
import api.repository.UserRepository;
import api.tools.jwt.JwtTokenProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = "http://localhost:8080")
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    FilesStorageService storageService;

    @Autowired
    ResponseProvider responseProvider;

    @Autowired
    PasswordUtil passwordUtil;

    @Autowired
    JwtTokenProvider tokenProvider;

    @GetMapping(value = "/users")
    public ResponseEntity<List<User>> getAllUsers() 
    {
        List<User> users = userRepository.getAllUsers();

        if(users == null) 
        {
            users = new ArrayList<User>();
        }

        ResponseEntity<List<User>> response = ResponseEntity.ok(users);

        return response;
        
    }

    @PostMapping(value = "/login")
    public ResponseEntity<BasicResponse> login(@RequestBody User user) 
    {
        if(user.getMail() == null || user.getMail().isEmpty()) {
            return ResponseEntity.badRequest().body(responseProvider.response(400, "Field email is empty", null));
        }

        if(user.getPassword() == null || user.getPassword().isEmpty()) {
            return ResponseEntity.badRequest().body(responseProvider.response(400, "Field password is empty", null));
        }

        String token = null;
        User userToLog = userRepository.findByEmail(user.getMail());
        String passwordToSend = passwordUtil.removeKeys(userToLog.getPassword());
        String passwordToCheck = passwordUtil.addKeys(user.getPassword());
        
        if (passwordUtil.checkPassword(passwordToSend, passwordToCheck))
        {
            userToLog.setPassword(passwordToCheck);
            token = userService.login(userToLog);
        }
        
        UserLoged userLoged = new UserLoged();
        userLoged.setMail(user.getMail());
        userLoged.setUsername(user.getUsername());
        userLoged.setProfilePicture(user.getProfilePicture());
        userLoged.setToken(token);

        if(userLoged.getToken() == null) {
            return ResponseEntity.badRequest().body(responseProvider.response(400, "Password incorrect", null));
        } else {
            return ResponseEntity.ok(responseProvider.response(200, "User login successfully", userLoged));
        }
        
    }

    @PostMapping(value = "/register")
    public ResponseEntity<Object> createUser(@RequestBody User user) 
    {
        Object newUser = userRepository.addUser(user);

        ResponseEntity<Object> response = ResponseEntity.ok(newUser);

        return response;
    }

    @PutMapping(value = "/update")
    public ResponseEntity<Object> updateUser(@RequestHeader("Authorization") String token, @RequestBody User user) 
    {
        ResponseEntity<Object> response;

        User existingUser = userRepository.findById(user.getId());
        if(existingUser != null) {
            user.setPassword(passwordUtil.encryptPassword(user.getPassword()));
            Boolean updatedUser = userRepository.update(user);
            response = ResponseEntity.ok(updatedUser);
        } else {
            response = ResponseEntity.badRequest().body(responseProvider.response(400, "User with this mail don't exist", null));
        }


        return response;
    }

    @PostMapping(value = "/upload-pp/{id}")
    public ResponseEntity<Object> uploadFile(@RequestParam("file") MultipartFile file, @PathVariable("id") String id) 
    {
        String filename = "profil-" + id + "." + getExtensionByStringHandling(file.getOriginalFilename()).get();

        String uploadedFile = userRepository.storeFile(file, filename, id);
        
        ResponseEntity<Object> response = ResponseEntity.ok(responseProvider.response(200, uploadedFile, null));

        return response;
    }

    @PostMapping(value = "/userData")
    public ResponseEntity<BasicResponse> getUserData(@RequestBody User user) 
    {
        
        try {
            User userData = userRepository.findByEmail(user.getMail());
            return ResponseEntity.ok(responseProvider.response(200, "User data recover successfully", userData));
        }
        catch(Exception e) {
            return ResponseEntity.badRequest().body(responseProvider.response(400, "User with this mail don't exist", e));
        }

    }

    public Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
          .filter(f -> f.contains("."))
          .map(f -> f.substring(filename.lastIndexOf(".") + 1));
      }
    
}
