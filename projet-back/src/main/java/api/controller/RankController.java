package api.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import api.models.Rank;
import api.repository.RankRepository;

@RestController
@RequestMapping(value = "/api")
public class RankController {

    @Autowired
    private RankRepository rankRepository;

    @GetMapping(value = "/ranks")
    public ResponseEntity<List<Rank>> getAllRanks() {

        List<Rank> ranks = rankRepository.getAllRanks();

        if(ranks == null) {
            ranks = new ArrayList<Rank>();
        }

        ResponseEntity<List<Rank>> response = ResponseEntity.ok(ranks);

        return response;
        
    }

    
    
    
    
}
