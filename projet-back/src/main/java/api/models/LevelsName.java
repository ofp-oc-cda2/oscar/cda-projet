package api.models;

import javax.persistence.*;

@Entity
@Table(name = "levels_names")
public class LevelsName {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false, length = 30)
    private String name;

    @Column(name = "level_need", nullable = false)
    private Integer levelNeed;

    public Integer getLevelNeed() {
        return levelNeed;
    }

    public void setLevelNeed(Integer levelNeed) {
        this.levelNeed = levelNeed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}