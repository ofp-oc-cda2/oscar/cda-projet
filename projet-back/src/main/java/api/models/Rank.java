package api.models;

import javax.persistence.*;

@Entity
@Table(name = "ranks")
public class Rank {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "points_need", nullable = false)
    private Integer pointsNeed;

    public Integer getPointsNeed() {
        return pointsNeed;
    }

    public void setPointsNeed(Integer pointsNeed) {
        this.pointsNeed = pointsNeed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}