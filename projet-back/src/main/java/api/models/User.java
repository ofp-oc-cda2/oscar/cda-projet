package api.models;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "username", nullable = false, length = 20)
    private String username;

    @Column(name = "mail", nullable = false, length = 50)
    private String mail;

    @Column(name = "password", nullable = false, length = 250)
    private String password;

    @Column(name = "points", nullable = false)
    private Integer points;

    @Column(name = "level", nullable = false)
    private Integer level;

    @Column(name = "exp", nullable = false)
    private Integer exp;

    @Column(name = "rank_id", nullable = false)
    private Integer rankId;

    @Column(name = "team_id", nullable = false)
    private Integer teamId;

    @Column(name = "profile_picture", nullable = false)
    private String profilePicture;

    @Column(name = "verif_email", nullable = false)
    private Boolean verifEmail = false;

    @Column(name = "date_create", nullable = false)
    private Date dateCreate;

    @Column(name = "money", nullable = false)
    private Integer money;


    public User() {
        
    }

    public User(int id, String username, String mail, String password, int points, int level, int exp, int rankId, int teamId, String profilePicture, boolean verifEmail, Date dateCreate, int money) {
        this.id = id;
        this.username = username;
        this.mail = mail;
        this.password = password;
        this.points = points;
        this.level = level;
        this.exp = exp;
        this.rankId = rankId;
        this.teamId = teamId;
        this.profilePicture = profilePicture;
        this.verifEmail = verifEmail;
        this.dateCreate = dateCreate;
        this.money = money;
    }
    

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Boolean getVerifEmail() {
        return verifEmail;
    }

    public void setVerifEmail(Boolean verifEmail) {
        this.verifEmail = verifEmail;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public Integer getRankId() {
        return rankId;
    }

    public void setRankId(Integer rankId) {
        this.rankId = rankId;
    }

    public Integer getExp() {
        return exp;
    }

    public void setExp(Integer exp) {
        this.exp = exp;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

}