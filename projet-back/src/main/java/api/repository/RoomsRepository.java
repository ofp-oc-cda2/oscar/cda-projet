package api.repository;

// import java.util.ArrayList;
// import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.models.Rooms;

@Repository
public class RoomsRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    @Transactional
    public Rooms getRoomByAccessCode(Integer accessCode) {

        final String GET_ROOM = "SELECT * FROM rooms WHERE code = :accesscode";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("accesscode", accessCode);
        Rooms room = jdbc.query(GET_ROOM, params,
                (rs) -> {
                    if (rs != null) {
                        Rooms rooms = new Rooms();
                        while (rs.next()) {
                        rooms.setId(rs.getInt("id"));
                        rooms.setName(rs.getString("name"));
                        rooms.setCreator(rs.getInt("creator"));
                        rooms.setGame(rs.getInt("game"));
                        }
                        return rooms;
                    } else {
                        return null;
                    }
                });
        return room;
    }

    @Transactional
    public String createRooms(String name, Integer code, Integer creator, Integer game) {

        final String INSERT_ROOM = "INSERT INTO rooms (name, code, creator, game) VALUES (:name, :code, :creator, :game)";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", name);
        params.addValue("code", code);
        params.addValue("creator", creator);
        params.addValue("game", game);
        
        jdbc.update(INSERT_ROOM, params);

        return "Room created successfully";
    }

    @Transactional
    public String deleteRooms(Integer id) {

        final String DELETE_ROOM = "DELETE FROM rooms WHERE id = :id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);
        
        jdbc.update(DELETE_ROOM, params);

        return "Room created successfully";
    }

}
