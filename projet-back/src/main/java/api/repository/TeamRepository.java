package api.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.models.Team;

@Repository
public class TeamRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    private final String GET_ALL_TEAMS = "SELECT * FROM teams";

    @Transactional
    public List<Team> getAllTeams() {

        List<Team> teams = jdbc.query(GET_ALL_TEAMS,
                (rs) -> {
                    List<Team> list = new ArrayList<Team>();
                    if (rs != null)
                    {
                        while (rs.next())
                        {
                            Team team = new Team();
                            team.setId(rs.getInt("id"));
                            team.setName(rs.getString("name"));
                            team.setProfilePicture(rs.getString("profile_picture"));
                            team.setShortCode(rs.getString("short_code"));
                            team.setPoints(rs.getInt("points"));
                            team.setRankId(rs.getInt("rank_id"));
                            team.setLevel(rs.getInt("level"));
                            team.setExp(rs.getInt("exp"));
                            list.add(team);
                        }
                    }
                    return list;
                });
        return teams;
    }
}
