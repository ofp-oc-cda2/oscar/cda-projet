package api.repository;

import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.models.LevelsName;

@Repository
public class LevelsNameRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    private final String GET_ALL_LEVELS_NAMES = "SELECT * FROM levels_names";

    @Transactional
    public List<LevelsName> getAllLevelsNames() {

        List<LevelsName> levelsNames = jdbc.query(GET_ALL_LEVELS_NAMES,
                (rs) -> {
                    List<LevelsName> list = new ArrayList<LevelsName>();
                    if (rs != null) {
                        while (rs.next()) {
                            LevelsName levelsName = new LevelsName();
                            levelsName.setId(rs.getInt("id"));
                            levelsName.setName(rs.getString("name"));
                            levelsName.setLevelNeed(rs.getInt("level_need"));
                            list.add(levelsName);
                        }
                    }
                    return list;
                });
        return levelsNames;
    }
    
    
}
