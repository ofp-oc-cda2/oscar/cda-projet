package api.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.models.Games;
import api.models.Games.GameCategory;

@Repository
public class GamesRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    @Transactional
    public List<Games> getAllGames() {

        final String GET_ALL_GAMES = "SELECT * FROM games";

        List<Games> games = jdbc.query(GET_ALL_GAMES,
                (rs) -> {
                    List<Games> list = new ArrayList<Games>();
                    if (rs != null) {
                        while (rs.next()) {
                            Games game = new Games();
                            game.setId(rs.getInt("id"));
                            game.setName(rs.getString("name"));
                            game.setPicture(rs.getString("picture"));
                            game.setCategory(GameCategory.valueOf(rs.getString("category")));
                            game.setDescription(rs.getString("description"));
                            list.add(game);
                        }
                    }
                    return list;
                });

        return games;
    }

    @Transactional
    public Games getGameById(String id) {

        final String GET_ALL_GAMES = "SELECT * FROM games WHERE id = :id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);

        Games game = jdbc.query(GET_ALL_GAMES, params,
                (rs) -> {
                    Games games = new Games();
                    if (rs != null) {
                        while (rs.next()) {
                            Games gameFound = new Games();
                            gameFound.setId(rs.getInt("id"));
                            gameFound.setName(rs.getString("name"));
                            gameFound.setPicture(rs.getString("picture"));
                            gameFound.setCategory(GameCategory.valueOf(rs.getString("category")));
                            gameFound.setDescription(rs.getString("description"));
                            games = gameFound;
                        }
                    }
                    return games;
                });

        return game;
    }

}
