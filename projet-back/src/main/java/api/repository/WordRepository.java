package api.repository;

import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.models.Word;

@Repository
public class WordRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    private final String GET_ALL_WORDS = "SELECT * FROM words";

    @Transactional
    public List<Word> getAllWords() {

        List<Word> words = jdbc.query(GET_ALL_WORDS,
                (rs) -> {
                    List<Word> list = new ArrayList<Word>();
                    if (rs != null) 
                    {
                        while (rs.next()) 
                        {
                            Word word = new Word();
                            word.setId(rs.getInt("id"));
                            word.setWord(rs.getString("word"));
                            list.add(word);
                        }
                    }
                    return list;
                });
        return words;
    }
    
}
