package api.repository;

import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import api.models.Rank;

@Repository
public class RankRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    private final String GET_ALL_RANKS = "SELECT * FROM ranks";

    @Transactional
    public List<Rank> getAllRanks() {

        List<Rank> ranks = jdbc.query(GET_ALL_RANKS,
                (rs) -> {
                    List<Rank> list = new ArrayList<Rank>();
                    if (rs != null) {
                        while (rs.next()) {
                            Rank rank = new Rank();
                            rank.setId(rs.getInt("id"));
                            rank.setName(rs.getString("name"));
                            rank.setPointsNeed(rs.getInt("points_need"));
                            list.add(rank);
                        }
                    }
                    return list;
                });
        return ranks;
    }
    
}
