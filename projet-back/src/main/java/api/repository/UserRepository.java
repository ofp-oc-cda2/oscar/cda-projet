package api.repository;

import java.util.ArrayList;
//import java.util.HashMap;
import java.util.List;
//import java.util.Map;

import org.hibernate.criterion.NotNullExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import api.models.User;
import api.tools.specialModel.UserLoged;
import api.tools.fileStorage.FilesStorageService;
import api.tools.password.PasswordUtil;

@Repository
public class UserRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbc;

    @Autowired
    private FilesStorageService storageService;

    @Autowired
    private PasswordUtil passwordUtil;

    //private static final Logger log = LoggerFactory.getLogger(UserRepository.class);
    
    @Transactional
    public List<User> getAllUsers() {
        
        final String GET_ALL_USERS = "SELECT * FROM users";

        List<User> users = jdbc.query(GET_ALL_USERS,
                (rs) -> {
                    List<User> list = new ArrayList<User>();
                    if (rs != null)
                    {
                        while (rs.next())
                        {
                            User user = new User(
                                rs.getInt("id"), 
                                rs.getString("username"), 
                                rs.getString("mail"), 
                                rs.getString("password"), 
                                rs.getInt("points"), 
                                rs.getInt("level"), 
                                rs.getInt("exp"), 
                                rs.getInt("rank_id"), 
                                rs.getInt("team_id"), 
                                rs.getString("profile_picture"), 
                                rs.getBoolean("verif_email"), 
                                rs.getDate("date_create"), 
                                rs.getInt("money")
                            );
                            list.add(user);
                        }
                    }
                    return list;
                });
        return users;
    }

    @Transactional
    public Object addUser(User user) {

        if (user.getUsername() == null || user.getMail() == null || user.getPassword() == null)
        {
            return null;
        }

        List<User> users = getAllUsers();
        for (User u : users)
        {
            if (u.getUsername().equals(user.getUsername()) || u.getMail().equals(user.getMail()))
            {
                return null;
            }
        }

        String sql = "INSERT INTO users (username, mail, password) VALUES (:username, :mail, :password)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("username", user.getUsername());
        params.addValue("mail", user.getMail());
        params.addValue("password", passwordUtil.encryptPassword(user.getPassword()));

        jdbc.update(sql, params);

        UserLoged userLogin = new UserLoged();

        return userLogin;
    }

    @Transactional
    public String storeFile(MultipartFile file, String filename, String userId) {

        if (file == null || file.isEmpty())
        {
            return null;
        }

        if (filename == null || filename.isEmpty())
        {
            return null;
        }

        String sql = "SELECT profile_picture FROM users WHERE id = :userid";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userid", userId);

        try {
            String actualProfilePicture = jdbc.queryForObject(sql, params, String.class);
            if (actualProfilePicture != null)
            {
                storageService.delete(actualProfilePicture);
            }
        } catch (Exception e) {
            return null;
        }

        sql = "UPDATE users SET profile_picture = :filename WHERE id = :userid";
        params = new MapSqlParameterSource();
        params.addValue("filename", filename);
        params.addValue("userid", userId);
        jdbc.update(sql, params);

        try {
            storageService.save(file, filename);
            return filename;
        } catch (Exception e) {
            return null;
        }

    }

    @Transactional
    public Boolean update (User user) {

        if (user == null || user.getId() == null || user.getMail() == null || user.getUsername() == null || user.getPassword() == null)
        {
            return null;
        }

        final String UPDATE_USER = "UPDATE users SET mail = :newMail, username = :newUsername, password = :newPassword WHERE id = :id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("newMail", user.getMail());
        params.addValue("newUsername", user.getUsername());
        params.addValue("newPassword", passwordUtil.encryptPassword(user.getPassword()));
        params.addValue("id", user.getId());

        try {
            jdbc.update(UPDATE_USER, params);
            return true;
        }
        catch(NullPointerException err) {
            System.out.println(err);
            return false;
        }
        
    }

    @Transactional
    public User findByEmail (String email) {
        final String sql = "SELECT * FROM users WHERE mail = :mail";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("mail", email);
        User user = jdbc.queryForObject(sql, params, new BeanPropertyRowMapper<>(User.class));
        return user;
    }

    @Transactional
    public User findById (Integer id) {
        final String sql = "SELECT * FROM users WHERE id = :id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);
        try {
            User user = jdbc.queryForObject(sql, params, new BeanPropertyRowMapper<>(User.class));
        return user;
        }
        catch(NullPointerException err) {
            System.out.println(err);
            return null;
        }
    }

    @Transactional
    public Boolean deleteById (Integer id) {
        final String DELETE_USER = "DELETE FROM users WHERE id = :id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);

        try {
            jdbc.update(DELETE_USER, params);
            return true;
        }
        catch(NullPointerException err) {
            System.out.println(err);
            return false;
        }
    }
        
}