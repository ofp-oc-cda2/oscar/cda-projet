package api.jpa;

import api.models.User;
import api.repository.UserRepository;
import org.assertj.core.util.IterableUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.ArrayList;
import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class UserTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    // Default test instance
    private final User testUser = new User();

    // Helper function to initialize some users in table
    private void persistLists() {
        entityManager.persistAndFlush(new User(1, "john.doe", "john.doe@example.com", "password", 0, 1, 0, 1, 1, "", false, new Date(0), 0));
        entityManager.persistAndFlush(new User(2, "fcvgtyhu.doe", "tfghyuj.doe@example.com", "password", 0, 1, 0, 1, 1, "", false, new Date(0), 0));
    }

    @Test
    public void canCreateUser() {
        userRepository.addUser(testUser);

        Iterable<User> users = userRepository.getAllUsers();
        assertEquals(1, IterableUtil.sizeOf(users));
        assertEquals("mail@test.fr", users.iterator().next().getMail());
        assertEquals("Username", users.iterator().next().getUsername());
    }

    @Test
    public void canGetByEmail() {
        User user = entityManager.persistAndFlush(testUser);

        User found = userRepository.findByEmail(user.getMail());
        assertTrue(found != null);
        assertEquals(user.getId(), found.getMail());
    }

    @Test
    public void cannotCreateUserWithExistingEmail() {
        entityManager.persistAndFlush(testUser);
        assertThrows(DataIntegrityViolationException.class, () -> userRepository.addUser(new User()));
    }

    @Test
    public void canUpdateMail() {
        userRepository.update(testUser);

        User found = userRepository.findByEmail("bruh@test.com");
        assertTrue(found != null);
    }

    @Test
    public void canDeleteById() {
        User user = entityManager.persistAndFlush(testUser);

        userRepository.deleteById(user.getId());
        assertTrue(userRepository.findById(user.getId()) == null);
    }

}
