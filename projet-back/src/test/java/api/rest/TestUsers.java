package api.rest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.*;


import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import api.tools.fileStorage.FilesStorageService;
import api.tools.jwt.JwtTokenProvider;
import api.tools.password.PasswordUtil;
import api.tools.response.ResponseProvider;
import api.tools.userService.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import api.controller.UserController;
import api.models.User;
import api.repository.UserRepository;

@WebMvcTest(UserController.class)
public class TestUsers {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private UserService userService;

    @MockBean
    private FilesStorageService storageService;

    @MockBean
    private ResponseProvider responseProvider;

    @MockBean
    PasswordUtil passwordUtil;

    @MockBean
    JwtTokenProvider tokenProvider;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void testGetAllUsers() throws Exception {
        List<User> expectedUsers = Arrays.asList(
                new User(1, "john.doe", "john.doe@example.com", "password", 0, 1, 0, 1, 1, "", false, new Date(0), 0),
                new User(2, "jane.doe", "jane.doe@example.com", "password", 0, 1, 0, 1, 1, "", false, new Date(0), 0)
        );
        when(userRepository.getAllUsers()).thenReturn(expectedUsers);
    
        MvcResult result = mockMvc.perform(get("/api/users"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
    
        TypeReference<List<User>> typeReference = new TypeReference<List<User>>() {};
        List<User> actualUsers = new ObjectMapper().readValue(result.getResponse().getContentAsString(), typeReference);
    
        assertEquals(expectedUsers, actualUsers);
    
        verify(userRepository).getAllUsers();
    }

    @Test
    public void testAddUser() throws Exception {
        User user = new User(0, "john.doe", "john.doe@example.com", "password", 0, 1, 0, 1, 1, "", false, new Date(0), 0);
    
        String userJson = new ObjectMapper().writeValueAsString(user);
    
        doNothing().when(userRepository).addUser(user);
    
        MvcResult result = mockMvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson))
                .andExpect(status().isOk())
                .andReturn();
    
        verify(userRepository).addUser(user);
    }
    
}