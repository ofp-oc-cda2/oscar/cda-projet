# CDA-Projet

## Idées

Jeu code name avec système de points, rank etc

## Techno

### Back-end

Api sois en Java/Springb boot, sois en PHP/Laravel, sois en Node.js/Express

### Front-end

Version Desktop/web probablement en Vue.js

Version Desktop/App, je ne sais pas encore

Version Mobile/App, flutter


### Diagramme
```mermaid
graph TD
    A[User] ==>|create or join room| B{Room}
    B ==> |Select team| C(TEAM RED)
    B ==> |Select team| D(TEAM BLUE)
    C ---> |Select role| E(Spy)
    C ---> |Select role| F(Agent)
    D ---> |Select role| E(Spy)
    D ---> |Select role| F(Agent)
    E ==> G{ }
    F ==> G
    G ==> |Game Start| H{Game}
    H ==> |Choose  25 words| I[Words]
    I --> J(8-9 red words)
    I --> K(8-9 blue words)
    I --> L(7 neutral words)
    I --> M(1 dark word)
    J --> N{ }
    K --> N
    M ===> |first other team find this words| O{Win}
    N ==> |if team find all its words| O
    O ==> |User of winner team, gain points, other team lost points| A
```
## MCD (premier jet)
```mermaid
classDiagram
users <|--|> teams
users <|--|> ranks
ranks <|--|> teams
users <|-- level_name

class users{
    -id: int
    -username: string
    -mail: string
    -exp: int
    -level: int
    -points: int
    -rank_id: int
    -team_id: int
    -profil_picture: string
    -verif_email: boolean
}

class teams{
    -id: int
    -name: string
    -picture: string
    -short_code: string
    -points: int
    -rank_id: int
    -level: int
    -exp: int
}

class ranks{
    -id: int
    -name: string
    -points_need: int
}

class level_name{
    id: int
    name: string
    level_need: int
}

class words {
    id: int
    word: string
}

```